function checkInfo() {

var userName = document.forms["contactUsForm"] ["name"].value;
var userPhone = document.forms["contactUsForm"] ["phone"].value;
var userEmail = document.forms["contactUsForm"] ["email"].value;
var otherChecked = document.forms["contactUsForm"] ["reasons"].value;
var additionalInfo = document.forms["contactUsForm"] ["additionalInfo"].value;
var daysToContact = document.forms["contactUsForm"] ["mon"].checked || document.forms["contactUsForm"] ["tue"].checked || document.forms["contactUsForm"] ["wed"].checked || document.forms["contactUsForm"] ["thu"].checked || document.forms["contactUsForm"] ["fri"].checked
var itemsNeeded = [];


    if (userName == undefined || userName == "") {
        itemsNeeded.push("Your name\n")
    }
    
    if ((userPhone == undefined || userPhone == "") && (userEmail == undefined || userEmail == "")){
        itemsNeeded.push("Either your phone number or email address\n")
    }
    
    if ((otherChecked == "OTHER") && (additionalInfo == "")){
        itemsNeeded.push("Reason for inquiry in the Additional Information box\n")
    }

    if (daysToContact == false) {
        itemsNeeded.push("Which days to contact you\n")
    }       
    
    if (itemsNeeded.length > 0){
    alert("Please include the following items:\n\n " + itemsNeeded.join(" "));
    return false;
    }

    

}
